/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

require([
    'jquery',
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    'domReady!'
], function ($, alert, $t) {
    if (
        typeof Resursbank_FetchStores !== 'function' ||
        !/^.*admin\/system_config\/edit\/.*section\/payment.*/.test(window.location.href)
    ) {
        return;
    }

    const fetcher = new Resursbank_FetchStores({
        /**
         *
         * @param {Error} message
         */
        errorHandler: function(message) {
            alert({
                title: 'Resurs Bank',
                content: message
            });

            if (fetcher.fetching) {
                fetcher.toggleFetch(false);
            }
        },

        onComplete: function() {
            alert({
                title: 'Resurs Bank',
                content: (typeof rbFetchStoresPleaseSelect === 'string') ?
                    rbFetchStoresPleaseSelect :
                    'Please select store.'
            });
        },

        onToggle: function(state) {
            if (state) {
                $(document).trigger('ajaxSend', [undefined, { showLoader: true }]);
            } else {
                $(document).trigger('ajaxComplete', [undefined, { showLoader: true }]);
            }
        },

        getSelectStoreElement: function() {
            let result = document.getElementsByName('groups[resursbank_section][groups][api][fields][store][value]')[0];

            if (result === null) {
                throw new Error(
                    (typeof rbMissingSelectStoreElement === 'string') ?
                        rbMissingSelectStoreElement :
                        'Missing store select element.'
                );
            }

            return result;
        },

        getSelectEnvironmentElement: function() {
            let result = document.getElementsByName('groups[resursbank_section][groups][api][fields][environment][value]')[0];

            if (result === null) {
                throw new Error(
                    (typeof rbStoresMissingEnvironmentSelect === 'string') ?
                        rbStoresMissingEnvironmentSelect :
                        'Missing environment select element.'
                );
            }

            return result;
        },

        getClientIdElement: function() {
            let el = document.getElementsByName(
                'groups[resursbank_section][groups][api][fields][client_id_' +
                rbFetchStoresGetApiFlow() + '_' +
                this.getSelectEnvironmentElement().value + '][value]'
            )[0];

            if (el === null) {
                throw new Error(
                    (typeof rbFetchStoresMissingClientId === 'string') ?
                        rbFetchStoresMissingClientId :
                        'Missing client ID input field.'
                );
            }

            return el;
        },

        getClientSecretElement: function() {
            let el = document.getElementsByName(
                'groups[resursbank_section][groups][api][fields][client_secret_' +
                rbFetchStoresGetApiFlow() +
                '_' +
                this.getSelectEnvironmentElement().value + '][value]'
            )[0];

            if (el === null) {
                throw new Error(
                    (typeof rbFetchStoresMissingClientSecret === 'string') ?
                        rbFetchStoresMissingClientSecret :
                        'Missing client secret input field.'
                );
            }

            return el;
        },

        getUrl: function() {
            return getResursBankFetchStoresUrl();
        }
    });

    $('#resursbank_fetch_stores_btn').on('click', fetcher.fetchStores.bind(fetcher));

    fetcher.setupEventListeners();
});
