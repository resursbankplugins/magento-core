<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api\Payment\Item\Validation;

/**
 * Validation routines for property "vatPct".
 */
class VatPct extends AbstractValidation implements ValidationInterface
{
    /**
     * @inheritDoc
     */
    public function validate(
        float $value = 0.0
    ): void {
        $this->isPositiveNumber($value);
    }
}
