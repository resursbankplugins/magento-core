<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Payment;

use Magento\Framework\DataObject;
use Magento\Payment\Model\Method\Adapter;
use Magento\Quote\Api\Data\CartInterface;
use Resursbank\Core\Model\PaymentMethod;
use Resursbank\Ecom\Config as EcomConfig;
use Resursbank\Ecom\Lib\Model\Callback\GetAddressRequest;
use Resursbank\Ecom\Lib\Order\CustomerType;
use Resursbank\Ecom\Module\Customer\Repository;
use Throwable;

use function is_array;

/**
 * Resurs Bank payment method adapter.
 *
 * NOTE: This class includes logic to filter out payment methods in checkout
 * based on the following:
 *
 * 1. Configured country code on billing address against country code associated
 * with API account.
 *
 * 2. Customer type (natural or legal) against on the company name in the
 * billing address.
 */
class Resursbank extends Adapter
{
    /**
     * Payment method code prefix.
     *
     * @var string
     */
    public const CODE_PREFIX = 'resursbank_';

    /**
     * Prefix for modern, Ecom based, payment methods.
     */
    public const ECOM_PREFIX = 'rb-ecom-';

    /**
     * Default payment method code.
     *
     * @var string
     */
    public const CODE = self::CODE_PREFIX . 'default';

    /**
     * @var PaymentMethod|null
     */
    private ?PaymentMethod $resursModel = null;

    /**
     * When we create an instance of this payment method we will assign an
     * instance of the Resurs Bank payment method model
     * (see Plugin/Payment/Helper/Data.php :: getMethod()). We will use this
     * model instance to collect information such as title and command flags.
     *
     * NOTE: We could achieve the same thing by dependency inject the repository
     * in this class, but that would mean we be required to make a complex
     * relay call to the parent constructor. Since the design pattern for the
     * payment method adapters keep changing we should avoid that for now.
     *
     * @param PaymentMethod $model
     */
    public function setResursModel(
        PaymentMethod $model
    ): void {
        $this->resursModel = $model;
    }

    /**
     * Get Resurs model.
     *
     * @return PaymentMethod|null
     */
    public function getResursModel(): ?PaymentMethod
    {
        return $this->resursModel;
    }

    /**
     * Fields min_order_total and max_order_total need to be read before adapter
     * information is made available to value handlers. At the time of writing
     * there is no way for us to resolve these values from our value handlers
     * when the payment methods are rendered. This is important since we do not
     * wish to render methods outside the scope of the min / max values.
     *
     * NOTE: The values are resolved in
     * Plugin/Payment/Helper/Data.php :: getResursModel() to support the custom
     * Swish value override without adding an overriding constructor to this
     * class (see the notes on setResursModel above for further information).
     *
     * @inheridoc
     * @param string $field
     * @param int|null $storeId
     * @return mixed
     */
    public function getConfigData($field, $storeId = null): mixed
    {
        $result = parent::getConfigData($field, $storeId);

        if ($this->resursModel === null) {
            return $result;
        }

        return match ($field) {
            'min_order_total' => $this->resursModel->getMinOrderTotal(),
            'max_order_total' => $this->resursModel->getMaxOrderTotal(),
            'sort_order' => $this->resursModel->getSortOrder(),
            default => $result,
        };
    }

    /**
     * Filter payment methods by country.
     *
     * NOTE: $country comes from billing address, not shipping. Please see
     * the isAvailable docblock for more information.
     *
     * NOTE: This is called from the payment method renderer to filter out
     * payment methods that are not available for the current country. It's
     * separated from our isAvailable method since it's specifically designed
     * for country filtering by Magento.
     *
     * NOTE: Type-safety not possible due to parent class.
     *
     * @param string $country
     * @return bool
     */
    public function canUseForCountry($country): bool
    {
        return $this->resursModel === null ||
            $this->resursModel->getSpecificCountry() === $country;
    }

    /**
     * Check method availability.
     *
     * Compare data in the billing address with data on the payment method
     * to determine if the payment method is available. Also checks min and
     * max purchase limit.
     *
     * NOTE: The address being checked is the billing address. This is because
     * the billing address is what Magento internally is using to filter payment
     * methods. Note that when you log in it's the default billing address in
     * your address book that is used. If you choose to change your billing
     * address in association with payment method selection, the payment methods
     * will be filtered again through an AJAX request.
     *
     * @param CartInterface|null $quote
     * @return array|bool|mixed|null
     */
    public function isAvailable(?CartInterface $quote = null): mixed
    {
        $address = $quote->getBillingAddress() ? : $quote->getShippingAddress();
        $grandTotal = $quote->getGrandTotal();

        if ($this->resursModel === null) {
            return parent::isAvailable(quote: $quote);
        }

        try {
            $types = $this->resursModel->getCustomerTypes();
            $search = (string)$address->getCompany() === '' ?
                CustomerType::NATURAL->value :
                CustomerType::LEGAL->value;

            if ($types !== null && !in_array(
                needle: $search,
                haystack: $types,
                strict: true
            )
            ) {
                return false;
            }
        } catch (Throwable $e) {
            $this->logError(error: $e);
        }

        if (!$this->verifyGrandTotal(grandTotal: (float)$grandTotal)) {
            return false;
        }

        return parent::isAvailable(quote: $quote);
    }

    /**
     * Verify that grandTotal isn't too high or too low.
     *
     * @param float $grandTotal
     * @return bool
     */
    private function verifyGrandTotal(float $grandTotal): bool
    {
        try {
            if ($grandTotal < $this->resursModel->getMinOrderTotal() ||
                $grandTotal > $this->resursModel->getMaxOrderTotal()) {
                return false;
            }
        } catch (Throwable $e) {
            $this->logError(error: $e);
        }

        return true;
    }

    /**
     * Log error using ECom.
     *
     * NOTE: This is to avoid DI:ing a logg helper, since this would require us
     * to re-define DI:s specified by the parent class and maintain potential
     * changes to that class constructor.
     *
     * @param Throwable $error
     * @return void
     */
    private function logError(Throwable $error): void
    {
        try {
            EcomConfig::getLogger()->error($error);
            // phpcs:ignore
        } catch (Throwable) {
            // Do nothing. Logging is not critical and parent logger is private.
        }
    }

    /**
     * Assign data to info model instance
     *
     * @param DataObject $data
     * @return $this
     */
    public function assignData(DataObject $data): self
    {
        $ssnData = $data->getData('additional_data');

        if (!is_array($ssnData) ||
            !isset($ssnData['gov_id']) ||
            !isset($ssnData['is_company'])
        ) {
            Repository::clearSsnData();
            return $this;
        }

        try {
            Repository::setSsnData(new GetAddressRequest(
                govId: $ssnData['gov_id'],
                customerType: $ssnData['is_company'] === '1' ?
                    CustomerType::LEGAL :
                    CustomerType::NATURAL
            ));
        } catch (Throwable $e) {
            $this->logError(error: $e);
        }

        return $this;
    }
}
