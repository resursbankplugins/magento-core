<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Ecom\Module\SupportInfo\Widget\SupportInfo as Widget;
use Magento\Framework\Module\PackageInfo;
use Resursbank\Core\Helper\Log;
use Magento\Framework\Filesystem\Directory\ReadFactory;
use Throwable;

/**
 * Displays the SupportInfo widget from Ecom.
 */
class SupportInfo extends Field
{
    /** @var null|Widget $widget */
    public readonly ?Widget $widget;

    /**
     * @param Context $context
     * @param Log $log
     * @param PackageInfo $packageInfo
     * @param ComponentRegistrar $componentRegistrar
     * @param ReadFactory $readFactory
     */
    public function __construct(
        Context $context,
        private readonly Log $log,
        private readonly PackageInfo $packageInfo,
        private readonly ComponentRegistrar $componentRegistrar,
        private readonly ReadFactory $readFactory
    ) {
        $this->setTemplate(
            template: 'Resursbank_Core::system/config/support-info.phtml'
        );

        parent::__construct($context);

        $this->renderWidget();
    }

    /**
     * Render widget to properties.
     *
     * @return void
     */
    private function renderWidget(): void
    {
        try {
            $moduleVersion = $this->getVersion();
            $requirements = $this->getPhpRequirements();

            $this->widget = new Widget(
                minimumPhpVersion: $requirements['lowest'],
                maximumPhpVersion: $requirements['highest'],
                pluginVersion: $moduleVersion
            );
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }
    }

    /**
     * Get module version.
     *
     * @return string
     */
    public function getVersion(): string
    {
        try {
            return 'Resursbank_Core: ' . $this->packageInfo->getVersion(
                moduleName: 'Resursbank_Core'
            );
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return '';
    }

    /**
     * Get widget content.
     *
     * @param AbstractElement $element
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @noinspection PhpMissingParentCallCommonInspection
     */
    protected function _getElementHtml(
        AbstractElement $element
    ): string {
        return $this->_toHtml();
    }

    /**
     * Retrieve PHP version requirements.
     *
     * @return string[]
     */
    private function getPhpRequirements(): array
    {
        $requirements = $this->loadPhpRequirements();
        $splitRequirements = explode(
            separator: '|',
            string: $requirements,
        );

        $lowest = PHP_INT_MAX . '.' . PHP_INT_MAX;
        $highest = '0.0';
        foreach ($splitRequirements as $version) {
            $isCaret = false;
            if (preg_match(pattern: '/^\^/', subject: $version)) {
                $version = preg_replace(
                    pattern: '/^\^/',
                    replacement: '',
                    subject: $version
                );
                $isCaret = true;
            }

            if (version_compare(
                version1: $version,
                version2: $lowest,
                operator: '<'
            )
            ) {
                $lowest = $version;
            }

            if ($isCaret) {
                $version = preg_replace(
                    pattern: '/\.\d+$/',
                    replacement: '',
                    subject: $version
                );
                $version .= '.' . PHP_INT_MAX;
            }

            if (version_compare(
                version1: $version,
                version2: $highest,
                operator: '>'
            )
            ) {
                $highest = $version;
            }
        }

        return [
            'lowest' => $lowest,
            'highest' => $highest
        ];
    }

    /**
     * Attempt to fetch required PHP version(s) from composer.json.
     *
     * @return string|null
     */
    private function loadPhpRequirements(): ?string
    {
        try {
            $path = $this->componentRegistrar->getPath(
                type: ComponentRegistrar::MODULE,
                componentName: 'Resursbank_Core'
            );

            if ($path === null) {
                throw new InvalidDataException(
                    __('rb-failed-to-resolve-module-path')
                );
            }

            $data = (array) json_decode(
                json: $this->readFactory->create($path)
                    ->readFile('composer.json'),
                associative: true,
                flags: JSON_THROW_ON_ERROR
            );

            if (array_key_exists(key: 'require', array: $data)) {
                foreach ($data['require'] as $key => $version) {
                    if ($key === 'php') {
                        return $version;
                    }
                }
            }
        } catch (Throwable $error) {
            $this->log->exception($error);
        }

        return null;
    }
}
