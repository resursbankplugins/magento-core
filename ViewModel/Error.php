<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\ViewModel;

use Resursbank\Core\ViewModel\Session\Checkout as CheckoutSession;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

/**
 * View model for showing errors.
 */
class Error implements ArgumentInterface
{
    /**
     * @var CheckoutSession
     */
    private CheckoutSession $checkoutSession;

    /**
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        CheckoutSession $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Check whether payment failed.
     *
     * @return bool
     */
    public function paymentFailed(): bool
    {
        if ($this->checkoutSession->getResursBankPaymentFailed() === true) {
            $this->checkoutSession->setResursBankPaymentFailed(value: false);
            return true;
        }

        return false;
    }

    /**
     * Get error message.
     *
     * @return string
     */
    public function getError(): string
    {
        return __('The payment failed. Please confirm the cart content'.
            ' and try a different payment method')->getText();
    }
}
